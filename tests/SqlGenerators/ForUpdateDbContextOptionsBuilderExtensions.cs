using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PostgresSamples.SqlGenerators
{
    public static class ForUpdateDbContextOptionsBuilderExtensions
    {
        public static IQueryable<T> ForUpdate<T>(this IQueryable<T> source) =>
            source.TagWith(ForUpdateInterceptor.QueryTag);

        //public static DbContextOptionsBuilder UseLockModifiers(this DbContextOptionsBuilder builder) =>
        //    builder.ReplaceService<IQuerySqlGeneratorFactory, NpgsqlQuerySqlGeneratorFactory, MagicTagQuerySqlGeneratorFactory>();

        public static DbContextOptionsBuilder UseLockModifiers(this DbContextOptionsBuilder builder) =>
            builder.AddInterceptors(new ForUpdateInterceptor());

        private class ForUpdateInterceptor : DbCommandInterceptor
        {
            public const string QueryPrefix = "-- Use for update";
            public const string QueryTag = "Use for update";

            public override InterceptionResult<DbDataReader> ReaderExecuting(DbCommand command, CommandEventData eventData, InterceptionResult<DbDataReader> result)
            {
                PatchCommand(command);
                return base.ReaderExecuting(command, eventData, result);
            }

            public override ValueTask<InterceptionResult<DbDataReader>> ReaderExecutingAsync(DbCommand command, CommandEventData eventData, InterceptionResult<DbDataReader> result, CancellationToken cancellationToken = default)
            {
                PatchCommand(command);
                return base.ReaderExecutingAsync(command, eventData, result, cancellationToken);
            }

            private void PatchCommand(DbCommand command)
            {
                if (command.CommandText.StartsWith(QueryPrefix, StringComparison.Ordinal))
                {
                    int index = command.CommandText.IndexOfAny(Environment.NewLine.ToCharArray(), QueryPrefix.Length);
                    command.CommandText = command.CommandText[index..].Trim() + Environment.NewLine + "FOR UPDATE";
                }
            }
        }
    }
}